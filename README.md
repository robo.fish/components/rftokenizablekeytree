# RFTokenizableKeyTree

This package provides a simple data structure for storing items that contain a tokenizable storage key.
The sequential tokens of the storage key determine the storage path within the data structure.
