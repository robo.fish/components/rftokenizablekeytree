// swift-tools-version:5.6

import PackageDescription

let package = Package(
	name: "RFTokenizableKeyTree",
	platforms: [.iOS(.v13), .macOS(.v10_15), .watchOS(.v6), .tvOS(.v13)],
	products: [
		.library(
			name: "RFTokenizableKeyTree",
			targets: ["RFTokenizableKeyTree"]),
	],
	dependencies: [],
	targets: [
		.target(
			name: "RFTokenizableKeyTree",
			dependencies: []),
		.testTarget(
			name: "RFTokenizableKeyTreeTests",
			dependencies: ["RFTokenizableKeyTree"]),
	]
)
