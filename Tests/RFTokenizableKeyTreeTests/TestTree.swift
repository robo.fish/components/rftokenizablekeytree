//  Copyright 2020-2022 Robo.Fish UG

import Foundation
import RFTokenizableKeyTree

typealias TestTree = RFTokenizableKeyTree<File>

struct File
{
	var path : Path
	var content : String

	init(_ path : String, _ content : String)
	{
		self.path = Path(path)
		self.content = content
	}
}

extension File : RFTokenizableKeyItem, Equatable, Hashable
{
	var tokenizableKey : Path { path }
	var description : String { content }

	func hash(into hasher: inout Hasher) {
		hasher.combine(path)
	}
}

struct Path : Equatable, Hashable
{
	var components : [String]
	private let _path : String

	init(_ path : String)
	{
		_path = path
		components = path.trimmingCharacters(in: .whitespaces).components(separatedBy: "/")
	}

	static func == (lhs: Path, rhs: Path) -> Bool
	{
		lhs.components == rhs.components
	}

	func hash(into hasher: inout Hasher)
	{
		hasher.combine(components)
	}
}

extension Path  : RFTokenizableKey
{
	var tokens : [String] { components }

	var description: String { _path }
}
