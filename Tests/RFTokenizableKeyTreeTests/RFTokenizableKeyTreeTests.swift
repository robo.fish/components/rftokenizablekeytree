//  Copyright 2020-2022 Robo.Fish UG

import XCTest
@testable import RFTokenizableKeyTree

final class RFTokenizedKeyTreeTests: XCTestCase
{
	static var allTests = [
		("testInitialization", testInitialization),
		("testInsertion", testItemInsertion),
		("testSearch", testSearch),
		("testFind", testFind)
	]

	func testInitialization()
	{
		var tree = TestTree(items: _testItems)
		XCTAssertEqual(tree.description, _expectedDescription1)
		tree = TestTree(items: _testItems2)
		XCTAssertEqual(tree.description, _expectedDescription2)
	}

	func testItemInsertion()
	{
		var tree = TestTree()
		tree.insert(File("rf/tokenized/key/tree", "A"))
		XCTAssertEqual(tree.description, "rf.tokenized.key.tree : A")
	}

	func testSearch()
	{
		let tree = TestTree(items: _testItems2)
		let searchPath = "Mail/Bundles/MailLoader.mailbundle/Contents"
		XCTAssertEqual(tree[Path(searchPath)], File(searchPath, "D"))
	}

	func testFind()
	{
		let tree = TestTree(items: _testItems2)
		XCTAssertEqual(tree.find{ $0.description == "J" }.count, 1)
		XCTAssertEqual(tree.find{ $0.path.description.contains("MailLoader_5") }.count, 4)
	}

	private let _testItems = Set([
		File("/home", "H"),
		File("/home/Videos", "V"),
		File("Videos/Vacation/Tokio/day2.mp4", "Tokio Day 2"),
		File("Videos/Vacation/Berlin/day1.mp4", "Berlin Day 1"),
		File("Documents/Recipes/Soups/Tomato.txt", "Tomato Soup")
	])

	private let _expectedDescription1 =
		"""
		.home : H
		.home.Videos : V
		Documents.Recipes.Soups.Tomato.txt : Tomato Soup
		Videos.Vacation.Berlin.day1.mp4 : Berlin Day 1
		Videos.Vacation.Tokio.day2.mp4 : Tokio Day 2
		"""

	private let _testItems2 = Set<File>([
		File("Mail/Bundles/MailLoader.mailbundle/Contents/MacOS", "E"),
		File("Mail/Bundles/MailLoader.mailbundle/Contents/Resources/en.lproj", "G"),
		File("Mail/Bundles/MailLoader.mailbundle/Contents/Resources", "F"),
		File("Mail/Bundles/MailLoader.mailbundle/Contents", "D"),
		File("Mail/Bundles/MailLoader.mailbundle", "C"),
		File("Mail/Bundles/MailLoader_5.mailbundle/Contents/_CodeSignature", "K"),
		File("Mail/Bundles/MailLoader_5.mailbundle/Contents/MacOS", "J"),
		File("Mail/Bundles/MailLoader_5.mailbundle/Contents", "I"),
		File("Mail/Bundles/MailLoader_5.mailbundle", "H"),
		File("Mail/Bundles", "B"),
		File("Mail", "A")
	])

	private let _expectedDescription2 =
		"""
		Mail : A
		Mail.Bundles : B
		Mail.Bundles.MailLoader.mailbundle : C
		Mail.Bundles.MailLoader.mailbundle.Contents : D
		Mail.Bundles.MailLoader.mailbundle.Contents.MacOS : E
		Mail.Bundles.MailLoader.mailbundle.Contents.Resources : F
		Mail.Bundles.MailLoader.mailbundle.Contents.Resources.en.lproj : G
		Mail.Bundles.MailLoader_5.mailbundle : H
		Mail.Bundles.MailLoader_5.mailbundle.Contents : I
		Mail.Bundles.MailLoader_5.mailbundle.Contents.MacOS : J
		Mail.Bundles.MailLoader_5.mailbundle.Contents._CodeSignature : K
		"""
}
