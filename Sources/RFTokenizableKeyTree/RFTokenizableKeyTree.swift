//  Copyright 2020-2022 Robo.Fish UG

import Foundation

/// Simple data structure for storing items that can be distinguished by a tokenizable key.
///
/// The insertion key is a property of the item inserted into the tree.
/// For this reason, the item can not be modified after it was inserted.
/// The key is tokenizable into a sequence that determines the path to the item within the tree.
///
/// Note that the tree does not balance itself. For good item access performance, design
/// the item key such that the token sequences are distributed as uniformly as possible.
public struct RFTokenizableKeyTree<Item : RFTokenizableKeyItem> : Codable
{
	private var _root = Node()

	public init(items : Set<Item> = [])
	{
		insert(items)
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.singleValueContainer()
		_root = try container.decode(Node.self)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.singleValueContainer()
		try container.encode(_root)
	}

	mutating public func insert(_ item : Item)
	{
		_root.insert(item, depth: 0)
	}

	mutating public func insert(_ items : Set<Item>)
	{
		items.forEach{ _root.insert($0, depth: 0) }
	}

	public subscript(key : Item.Key) -> Item?
	{
		_root.findItem(for: key)
	}

	public func find(where matcher : (Item)->(Bool)) -> [Item]
	{
		_root.find(where: matcher)
	}

	struct Node : Codable
	{
		var children = [Item.Key.Token : Node]()
		var item : Item? = nil

		mutating func insert(_ item : Item, depth : Int)
		{
			if item.tokenizableKey.tokens.count == depth
			{
				self.item = item
			}
			else
			{
				let component = item.tokenizableKey.tokens[depth]
				var childNode = children[component] ?? Node()
				childNode.insert(item, depth: depth + 1)
				children[component] = childNode
			}
		}

		func findItem(for key : Item.Key, depth : Int = 0) -> Item?
		{
			if key.tokens.count == depth { return item }
			else if let child = children[key.tokens[depth]]
			{
				return child.findItem(for: key, depth: depth + 1)
			}
			return nil
		}

		func find(where matcher : (Item)->(Bool)) -> [Item]
		{
			var result = children.values.map{ $0.find(where: matcher) }.flatMap{ $0 }
			if let item_ = item, matcher(item_)
			{
				result.append(item_)
			}
			return result
		}

		func collectDescription(_ collection : inout [String])
		{
			if item != nil { collection.append("\(item!.tokenizableKey.tokens.map{$0.description}.joined(separator: ".")) : \(item!.description)") }
			children.sorted(by: {$0.0 < $1.0}).forEach{ $0.1.collectDescription(&collection) }
		}
	}
}

extension RFTokenizableKeyTree : CustomStringConvertible
{
	public var description: String
	{
		var allItemDescriptions = [String]()
		_root.collectDescription(&allItemDescriptions)
		return allItemDescriptions.joined(separator: "\n")
	}
}
