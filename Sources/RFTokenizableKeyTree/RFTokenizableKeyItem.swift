//  Copyright 2020 Robo.Fish UG

public protocol RFTokenizableKeyItem : Codable, Hashable, CustomStringConvertible
{
	associatedtype Key : RFTokenizableKey
	var tokenizableKey : Key { get }
}
