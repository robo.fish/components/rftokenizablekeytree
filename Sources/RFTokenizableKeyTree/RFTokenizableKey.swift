//  Copyright 2020 Robo.Fish UG

public protocol RFTokenizableKey : Codable
{
	associatedtype Token : Comparable, Codable, Hashable, CustomStringConvertible
	var tokens : [Token] { get }
}
